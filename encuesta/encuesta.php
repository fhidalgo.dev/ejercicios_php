<?php
	
	// $archivo1 = file("encuesta.csv");  # Sin sentido esta declaración
	$id = fopen("encuesta.csv", "a");
	$lineas1 = count($id);

	if($lineas1)
	{
		$cadena = $_POST['encuesta']."\n";
	}
	else
	{
		$cadena = $_POST['encuesta'];
	}

	//$cadena = ";".$_SERVER['REMOTE_ADDR'].";".date("d/m/Y h:i:s a");
	fwrite($id, $cadena);
	fclose($id);
	
	$excelente = 0;
	$buena = 0;
	$regular = 0;
	$mala = 0;
	$archivo = file("encuesta.csv");
	$lineas = count($archivo);
	for ($i = 0; $i < $lineas; $i++) 
	{ 
		$arr = explode(";", $archivo[$i]); // Tengo duda si esto es necesario "PREGUNTAR"
		switch (trim($arr[0])) 
		{
			case 'excelente':
				$excelente++;
				break;
			case 'buena':
				$buena++;
				break;
			case 'regular':
				$regular++;
				break;
			case 'mala':
				$mala++;
				break;
		}
	}

// print $_POST['encuesta']; 

?>

<table border="1" align="center"
cellpadding="6">
	<tr>
		<th colspan="4" align="center"><b>Resultados de </b><?php print $lineas; ?> Cargas</th>
	</tr>
	<tr>
		<td><b>Excelente</b></td>
		<td><b>Buena</b></td>
		<td><b>Regular</b></td>
		<td><b>Mala</b></td>
	</tr>
	<tr>
		<td><?php print $excelente."-".number_format(($excelente * 100 / $lineas),2,",","."); ?></td>
		<td><?php print $buena."-".number_format(($buena * 100 / $lineas),2,",","."); ?></td>
		<td><?php print $regular."-".number_format(($regular * 100 / $lineas),2,",","."); ?></td>
		<td><?php print $mala."-".number_format(($mala * 100 / $lineas),2,",","."); ?></td>
	</tr>
	<tr>
		<td colspan="4" align="center"><a href="encuesta.html">Atrás</a></td>
    </tr>
</table>