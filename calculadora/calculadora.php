<?php
	
	// Queda pendiente verificar las letras
	function Sumar($num1, $num2)
	{
		$total = $num1 + $num2;
		return $total;
	}

	function Restar($num1, $num2)
	{
		$total = $num1 - $num2;
		return $total;
	}

	function Multiplicar($num1, $num2)
	{
		$total = $num1 * $num2;
		return $total;
	}

	function Dividir($num1, $num2)
	{
		$total = $num1 / $num2;
		return $total;
	}

	if($_POST['numero1'] AND $_POST['numero2'] AND $_POST['tipo_operacion']) 
	{
		switch ($_POST['tipo_operacion']) 
		{
			case 1:
				$total = Sumar($_POST['numero1'], $_POST['numero2']);
				break;
			case 2:
				$total = Restar($_POST['numero1'], $_POST['numero2']);
				break;
			case 3:
				$total = Multiplicar($_POST['numero1'], $_POST['numero2']);
				break;
			case 4:
				$total = Dividir($_POST['numero1'], $_POST['numero2']);
				break;
		}
		
		include 'calculadora.html';
	}

	else
	{
		print "Debe llenar todos los campos";
	}
?>