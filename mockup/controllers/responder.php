<?php
	
	if (isset($_POST['nombre']))
	{
		include 'conexion.php';
		$fecha = date("y-m-d");
		$hora = date("H-i-s");
		$sql ="INSERT INTO comentarios (id, nombre, tema, fecha, hora, id_tema) VALUES ("."default,'".$_POST["nombre"]."','".$_POST["tema"]."','".$fecha."','".$hora."','".$_POST["oculto"]."')";;
		$consulta = mysqli_query($conexion, $sql);

		include '../templates/header.php';
		if (!mysqli_error($conexion)) 
		{
?>
			<script>
				Swal.fire({
							title: 'Satisfactorio!',
							text: 'Comentario agregado correctamente',
							icon: 'success',
							confirmButtonText: '<a href="../views/tema_views.php?id=<?php echo $_POST["oculto"]; ?>">Aceptar</a>',
							allowOutsideClick: false, // Permite que el usuario de click fuera de la alerta para cerrarla
							allowEscapeKey: false, // Permite que el usuario presione la tecla de esc para cerrar la alerta
							allowEnterKey: false, // Permite que el usuario presione la tecla de enter para cerrar la alerta
						});
			</script>
<?php
		}
		else
		{
			print mysqli_error($conexion);
?>
			<script>
				alert("Estamos en Mantenimiento");
			</script>
<?php
		}
		mysqli_close($conexion);
	}
?>