<?php

	$pt_lapices = 0;
	$pt_borradores = 0;
	$pt_reglas = 0;

	$ds_lapices = 0;
	$ds_borradores = 0;
	$ds_reglas = 0;
	

	function descuento($precio, $cantidad)
	{
		if ($cantidad < 20) { $porc = 0; }
		elseif ($cantidad >= 20 && $cantidad < 50) { $porc = 5; }
		elseif ($cantidad >= 50 && $cantidad < 100) { $porc = 10; }
		elseif ($cantidad >= 100) { $porc = 15; }

		$precio_producto = $cantidad * $precio;
		$descuento = (($cantidad * $precio) * $porc) / 100;
		$precio_descuento = $precio_producto - $descuento;

		return array($precio_producto, $descuento, $precio_descuento);
	}

?>
