Swal.fire({
	title: 'Satisfactorio!',
	text: 'Mensaje agregado correctamente',
	icon: 'success',
	confirmButtonText: '<a href="../views/foro_views.php">Aceptar</a>',
	allowOutsideClick: false, // Permite que el usuario de click fuera de la alerta para cerrarla
	allowEscapeKey: false, // Permite que el usuario presione la tecla de esc para cerrar la alerta
	allowEnterKey: false, // Permite que el usuario presione la tecla de enter para cerrar la alerta
});