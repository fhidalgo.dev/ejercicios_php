<?php 
	include '../templates/header.php'; 
	include '../controllers/orden_compra.php'; 
?>

<div class="container">
	<div class="row text-white bg-primary ">

		<div class="col-sm-12 col-md-12 col-lg-12">
	        <h1 align="center">Orden de Compra</h1>
	        <hr/>
		</div>	
		<div class="col-sm-12 col-md-12 col-lg-6">
			<table border="1" cellpadding="6" align="center">
				<tr>
					<th>Productos</th>
					<th>Cantidad</th>
					<th>Precio Total de Producto</th>
				</tr>
				<tr>
					<td>
						Lapices
					</td>
					<td>
						<input type="number" name="cant_lapices" size="4" readonly value="<?php print $_POST['cant_lapices']; ?>">
					</td>
					<td>
					<?php

						if (isset($_POST['cant_lapices']) && isset($_POST['lapices'])) 
						{
							$resultado = descuento($_POST['lapices'], $_POST['cant_lapices']);
							$pt_lapices = $resultado[0];
							$ds_lapices = $resultado[1];
							print $pd_lapices = $resultado[2];
						}
						else 
						{
							print 0;
						}
					?>
					</td>
				</tr>
				<tr>
					<td>
						Borradores
					</td>
					<td>
						<input type="number" name="cant_borradores" size="4" readonly value="<?php print $_POST['cant_borradores']; ?>">
					</td>
					<td>
					<?php

						if (isset($_POST['cant_borradores']) && isset($_POST['borradores'])) 
						{
							$resultado = descuento($_POST['borradores'], $_POST['cant_borradores']);
							$pt_borradores = $resultado[0];
							$ds_borradores = $resultado[1];
							print $pd_borradores = $resultado[2];
						}
						else 
						{
							print 0;
						}
					?>
					</td>
				</tr>
				<tr>
					<td>
						Reglas
					</td>
					<td>
						<input type="number" name="cant_reglas" size="4" readonly value="<?php print $_POST['cant_reglas']; ?>">
					</td>
					<td>
					<?php

						if (isset($_POST['cant_reglas']) && isset($_POST['reglas'])) 
						{
							$resultado = descuento($_POST['reglas'], $_POST['cant_reglas']);
							$pt_reglas = $resultado[0];
							$ds_reglas = $resultado[1];
							print $pd_reglas = $resultado[2];
						}
						else 
						{
							print 0;
						}
					?>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<center>
						<button type="button" name="enviar" id="enviar" class="btn btn-outline-light">
							<a href="orden_compra_views.php">Borrar</a>
						</button>
						</center>	
					</td>
				</tr>
			</table>

			Por cada 20 unidades 5% de descuento<br/>
			Por cada 50 unidades 10% de descuento<br/>
			Por cada 100 unidades 15% de descuento<br/>
			<hr/>
		</div>
		<div class="col-sm-12 col-md-12 col-lg-6">

        <div class="form-row">
			<div class="form-group col-sm-6 col-md-6 col-lg-6">
				<label for="total"><b>Total</b></label>
				<input type="number" name="total" class="form-control" size="4" readonly value="<?php print $total = $pt_lapices + $pt_borradores + $pt_reglas; ?>">
			</div>
			<div class="form-group col-sm-6 col-md-6 col-lg-6">
				<label for="total"><b>Descuento</b></label>
				<input type="number" name="descuento" class="form-control" size="4" readonly value="<?php print $descuento = $ds_lapices + $ds_borradores + $ds_reglas; ?>">
			</div>

			<div class="form-group col-sm-6 col-md-6 col-lg-6">
				<label for="iva"><b>Iva</b></label>
				<input type="number" name="iva" class="form-control" size="4" readonly value="<?php print $iva = (($total - $descuento) * 12) / 100; ?>">
			</div>
			<div class="form-group col-sm-6 col-md-6 col-lg-6">
				<label for="total_pagar"><b>Total a Pagar</b></label>
				<input type="number" name="total_pagar" class="form-control" size="4" readonly value="<?php print $pagar = $total + $descuento + $iva; ?>">
			</div>
		</div>
		</div>
		
	</div>
</div>

<?php include '../templates/footer.html'; ?>