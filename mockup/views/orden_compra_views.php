<?php include '../templates/header.php'; ?>

<script src="../assets/js/alerta_orden_compra.js"></script>

<div class="container">
  <div class="row text-white bg-primary align-items-center">
    <div class="col-sm-12 col-md-12 col-lg-12 ">

      <form action="resultado_orden_compra_views.php" method="post">
        <h1 align="center">Orden de Compra</h1>
        <hr/>
        <table border="1" cellpadding="6">
          <tr>
            <th><b>Productos</b></th>
            <th><b>Cantidad</b></th>
            <th><b>Precio</b></th>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="lapices" value="1200">Lapices
            </td>
            <td>
              <input type="number" name="cant_lapices" size="4">
            </td>
            <td>
              1200
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="borradores" size="4" value="1600">Borradores
            </td>
            <td>
              <input type="number" name="cant_borradores" size="4">
            </td>
            <td>
              1600
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="reglas" size="4" value="2100">Reglas
            </td>
            <td>
              <input type="number" name="cant_reglas" size="4">
            </td>
            <td>
              2100
            </td>
          </tr>
          <tr>
            <td colspan="3">
              <button type="submit" name="enviar" id="enviar" class="btn float-left btn-outline-light">
                Enviar
              </button>
              <button type="reset" name="borrar" id="borrar"  class="btn float-right btn-outline-light">
                Borrar
              </button>
            </td>
          </tr>
        </table>
      </form>

      Por cada 20 unidades 5% de descuento<br/>
      Por cada 50 unidades 10% de descuento<br/>
      Por cada 100 unidades 15% de descuento<br/>

    </div>
  </div>
</div>

<?php include '../templates/footer.html'; ?>