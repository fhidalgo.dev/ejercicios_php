<?php include '../templates/header.php'; ?>

<div class="container">
	<div class="row text-white bg-primary align-items-center">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<table border="1" align="center" cellpadding="6">
				<tr>
					<th colspan="4" align="center"><b>Resultados de </b><?php print $lineas; ?> Cargas</th>
				</tr>
				<tr>
					<td><b>Excelente</b></td>
					<td><b>Buena</b></td>
					<td><b>Regular</b></td>
					<td><b>Mala</b></td>
				</tr>
				<tr>
					<td><?php print $excelente."-".number_format(($excelente * 100 / $lineas),2,",","."); ?></td>
					<td><?php print $buena."-".number_format(($buena * 100 / $lineas),2,",","."); ?></td>
					<td><?php print $regular."-".number_format(($regular * 100 / $lineas),2,",","."); ?></td>
					<td><?php print $mala."-".number_format(($mala * 100 / $lineas),2,",","."); ?></td>
				</tr>
				<tr>
					<td colspan="4" align="center"><a href="../views/encuesta_views.php">Atrás</a></td>
			    </tr>
			</table>
		</div>
	</div>
</div>

<?php include '../templates/footer.html'; ?>
