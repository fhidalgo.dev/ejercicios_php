<?php include '../templates/header.php'; ?>

<div class="container">
	<div class="row text-white bg-primary align-items-center">
		
		<div class="col-sm-12 col-md-12 col-lg-12">
	        <h1 align="center">Tema Nuevo</h1>
	        <hr/>
		</div>

		<div class="col-sm-12 col-md-12 col-lg-12">
			<form action="../controllers/add_tema.php" method="post">
		        <div class="form-row">
					<div class="form-group col-sm-12 col-md-12 col-lg-12">
						<label for="nombre"><b>Nombre</b></label>
			    		<input type="text" required="True" class="form-control" name="nombre"/>
					</div>
					<div class="form-group col-sm-12 col-md-12 col-lg-12">
						<label for="tema"><b>Tema</b></label>
					   	<textarea name="tema" required="True" class="form-control"></textarea>
					</div>
				</div>
				<button type="button" name="mostrar_temas" id="mostrar_temas" class="btn btn-outline-light float-left">
					<a href="foro_views.php">Mostrar Temas</a>
				</button>
				<button type="submit" name="enviar" id="enviar"  class="btn btn-outline-light float-right">
					Enviar
				</button>
			</form>
		</div>
	</div>
</div>

<?php include '../templates/footer.html'; ?>
