<?php
	
	include '../templates/header.php';
	include '../controllers/conexion.php';

	if (isset($_GET['id'])) 
	{
		$sql = "SELECT * FROM temas WHERE id = '".$_GET["id"]."'"; 
	}
	else
	{
		$sql = "SELECT * FROM temas WHERE id = '".$_POST["oculto"]."'";
	}
	$ver = mysqli_fetch_array(mysqli_query($conexion, $sql));
?>

<div class="container">
	<div class="row text-white bg-primary align-items-center">
		
		<div class="col-sm-12 col-md-12 col-lg-12">
	        <h1 align="center">Tema: <?php print $ver[2] ?></h1>
	        <hr/>
		</div>

		<div class="col-sm-12 col-md-12 col-lg-12">
			<form action="../controllers/responder.php" method="post">
				<input name="oculto" type="hidden" id="oculto" value="<?php print $_GET['id']; ?>">

		        <div class="form-row">
					<div class="form-group col-sm-12 col-md-12 col-lg-12">
						<label for="nombre"><b>Nombre</b></label>
						<input type="text" class="form-control" name="nombre">
					</div>
					<div class="form-group col-sm-12 col-md-12 col-lg-12">
						<label for="nombre"><b>Comentario</b></label>
						<textarea class="form-control" name="tema"></textarea>
					</div>
				</div>

				<div class="text-center">
	            	<button type="submit" name="enviar" id="enviar" class="btn btn-outline-light">
	            		Enviar
	            	</button>
				</div>

			</form>
		</div>

	</div>
</div>

<?php include '../templates/footer.html'; ?>
