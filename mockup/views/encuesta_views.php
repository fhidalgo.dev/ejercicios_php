<?php include '../templates/header.php'; ?>

<div class="container">
  <div class="row text-white bg-primary align-items-center">
    <div class="col-sm-12 col-md-12 col-lg-12">    
      <form action="../controllers/encuesta.php" method="post" enctype="multipart/form-data">
        <h1 align="center">Encuesta</h1>
        <hr/>  
        <p align="center">¿Qué Piensas del Futuro de Venezuela?</p>
          <label>Excelente</label>
          <input type="radio" name="encuesta" value="excelente"><br/>
          <label>Bueno</label>
          <input type="radio" name="encuesta" value="buena"><br/>
          <label>Regular</label>
          <input type="radio" name="encuesta" value="regular"><br/>
          <label>Malo</label>
          <input type="radio" name="encuesta" value="mala"><br/>
          <center><input type="submit" name="enviar" value="Enviar"></center>
      </form>
    </div>
  </div>
</div>

  </body>
</html>

<?php include '../templates/footer.html'; ?>
