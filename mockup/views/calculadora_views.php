<?php include '../templates/header.php'; ?>

<div class="container">
	<div class="row text-white bg-primary align-items-center">
		<div class="col-sm-12 col-md-12 col-lg-12">
      <h1 align="center">Calculadora</h1>

        <form action="../controllers/calculadora.php" method="post" enctype="multipart/form-data">
          <label for="numero1">Número 1</label>
          <input type="number" name="numero1" id="numero1" required="True">
          <label for="numero2">Número 2</label>
          <input type="number" name="numero2" id="numero2" required="True">
          <hr/>
          <h2>Tipo de Operación</h2>
          <label><input type="radio" name="tipo_operacion" value="1">Sumar</label><br/>
          <label><input type="radio" name="tipo_operacion" value="2">Restar</label><br/>
          <label><input type="radio" name="tipo_operacion" value="3">Multiplicar</label><br/>
          <label><input type="radio" name="tipo_operacion" value="4">Dividir</label><br/>
          <h2>Resultado</h2>
          <input type="number" name="resultado" readonly="True" placeholder="Aquí verá el resultado" value="<?php print $total; ?>">       
          <center><button type="submit" name="calcular" id="calcular"  class="btn btn-outline-light">
            Calcular
          </button></center>

        </form>

    </div>
  </div>
</div>

<?php include '../templates/footer.html'; ?>
