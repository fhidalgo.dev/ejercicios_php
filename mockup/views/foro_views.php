<?php
	include '../templates/header.php';
	include '../controllers/conexion.php';
?>

<div class="container">
	<div class="row text-white bg-primary align-items-center">
		<div class="col-sm-12 col-md-12 col-lg-12 table-responsive">
			<h1 align="center">Foro</h1>
			<hr/>

			<?php  

				$sql = "SELECT id, nombre, fecha, tema FROM temas ORDER BY fecha, hora DESC";
				$consulta = mysqli_query($conexion, $sql); 

			?>
			<table class="table table-hover" border="1" cellpadding="3" cellspacing="1">
				<thead class="thead-dark">
				<tr align="center">
					<th>Tema</th>
					<th>Fecha</th>
					<th>Respuesta</th>
				</tr>
				</thead>

				<?php
					while ($fila = mysqli_fetch_array($consulta)) 
					{ 
				?>
					<tr>
						<td><a href="tema_views.php?id=<?php print $fila[0]; ?>"><?php print $fila[1] ?></a></td>
						<td><?php print $fila[2] ?></td>
						<td><?php $buscar = "SELECT id FROM comentarios WHERE tema_id = '$fila[0]'";
								  $con = mysqli_query($conexion, $buscar);
								  $mostrar;
								  print $fila[3];
								  ?></td>
					</tr>
				<?php
					}
					mysqli_close($conexion);
				?>
			</table>

			<center><button type="submit" name="calcular" id="calcular" class="btn btn-outline-light">
				<a href="add_tema_views.php">Agregar Un Tema Nuevo</a>
			</button></center>
		</div>
	</div>
</div>

<?php include '../templates/footer.html'; ?>
