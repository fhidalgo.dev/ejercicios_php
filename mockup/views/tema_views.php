<?php
	
	include '../templates/header.php';
	include '../controllers/conexion.php';

	$sql = "SELECT * FROM temas WHERE id = ".$_GET['id']."";
	$ver = mysqli_fetch_array(mysqli_query($conexion, $sql));
	$comentarios = "SELECT * FROM comentarios WHERE id_tema = '".$_GET['id']."' ORDER BY id DESC";
	$consulta = mysqli_query($conexion, $comentarios);
?>

<div class="container">
	<div class="row text-white bg-primary align-items-center">
		
		<div class="col-sm-12 col-md-12 col-lg-12">
	        <h1 align="center">Tema</h1>
	        <hr/>
		</div>

		<div class="col-sm-12 col-md-12 col-lg-12">
			<table class="table table-hover" border="1" cellpadding="3" cellspacing="1">
				<thead class="thead-dark">
				<tr align="center">
					<td><a href="responder_views.php?id=<?php print $_GET['id']; ?>">Responder</a></td>
					<td>
						<a href="foro_views.php">Regresar al Foro</a>
					</td>
					<td>Fecha y Hora</td>
				</tr>
				<thead class="thead-dark">
				<tr>
					<td>Tema:<br><?php print $ver[2]; ?> </td>
					<td>Autor:<br><?php print $ver[1]; ?> </td>
					<td>Fecha y Hora:<br><?php print substr($ver[3],8,2).substr($ver[3],0,4); ?> </td>
				</tr>
				<?php 
					while ($res = mysqli_fetch_array($consulta))
					{ 
				?>
				<tr>
					<td>Responde:<?php print $res[1]; ?></td>
					<td>Respuesta:<?php print $res[2]; ?></td>
					<td>Fecha y Hora:<?php print substr($ver[3],4,4).substr($ver[3],0,4); ?>-<?php print $ver[4]; ?></td>
				</tr>
				<?php
					}
					mysqli_close($conexion); 
				?>
			</table>
		</div>
	</div>
</div>

<?php include '../templates/footer.html'; ?>
