<?php

  /** Ruta del proyecto */
  define('BASE_DIR', __DIR__);

  /** Ruta en el navegador */
  define('BASE_URL', 'http://localhost/mockup');
  /** Se definen dichas constantes para que no haya problema con las rutas si 
  se incluyen en distintas carpetas */

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <!--Metaetiqueta de Ventana Gráfica Receptiva -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ejecicios PHP 7.0</title>
    <!-- Inclusiones de Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php print BASE_URL.'/assets/css/bootstrap.css'; ?>" media="all">
    <link rel="stylesheet" type="text/css" href="<?php print BASE_URL.'/assets/css/bootstrap-grid.css'; ?>" media="all">
    <link rel="stylesheet" type="text/css" href="<?php print BASE_URL.'/assets/css/bootstrap-reboot.css'; ?>" media="all">
    <!-- Inclusiones de Propias CSS -->
    <link rel="stylesheet" type="text/css" href="<?php print BASE_URL.'/assets/css/mockup.css'; ?>" media="all">
    <!-- Inclusiones de Bootstrap Js -->
    <script type="text/javascript" src="<?php print BASE_URL.'/assets/js/bootstrap.js'; ?>"></script>

  </head>
  <body>
    
    <!-- Cabecera -->
    <div class="container-fluid py-2 text-center">
        <div class="row text-white bg-primary align-items-center">
          <div class="col-sm-12 col-md-12 col-lg-2">
            <img src="<?php print BASE_URL.'/assets/img/logo.svg'; ?>" alt="Logo de la Sección" width="40" height="40">            
          </div>
          <div class="col-sm-12 col-md-12 col-lg-8">
            <h1>Ejercicios PHP 7.0</h1>
          </div>
        </div>
    </div>
    <!-- Fin de Cabecera -->
    <hr/>
    <!-- Menú -->

    <div class="container-fluid py-2 text-center">
      <nav class="navbar sticky-top navbar-dark bg-dark navbar-expand-sm">
        <a class="navbar-brand text-white" href="<?php print BASE_URL.'/index.php'; ?>">
          Sección 302
          <img src="<?php print BASE_URL.'/assets/img/logo.svg'; ?>" alt="Logo Estudiantes" width="40" height="40" />
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">        
          <div class="navbar-nav ml-auto mr-auto text-center">
            <a class="nav-link active text-white" href="<?php print BASE_URL.'/index.php'; ?>">Inicio</a>
          </div>
        </div>
      </nav>
    </div>
    <!-- Fin de Menú -->


    <!-- Optional JavaScript - Requerido para el menú en torta -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="<?php print BASE_URL.'/assets/js/jquery-3.4.1.slim.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php print BASE_URL.'/assets/js/popper.min.js'; ?>"></script>
    <script src="<?php print BASE_URL.'/assets/js/bootstrap.min2.js'; ?>"></script>
    <!-- Libreria js SweetAler2 para estilizar las alertas convencionales de Javascript -->
    <script type="text/javascript" src="<?php print BASE_URL.'/assets/js/lib/sweetalert2@9.js'; ?>"></script>
